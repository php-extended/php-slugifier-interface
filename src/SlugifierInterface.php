<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

use Stringable;

/**
 * SlugifierInterface interface file.
 * 
 * This interface specifies how a slugifier should work. A slugifier is a 
 * string transliterator that transforms strings according to specific rules,
 * most of the time to have it in a simplified character set (ascii) or to be
 * easily readable for anyone, or to be put in urls.
 * 
 * @author Anastaszor
 */
interface SlugifierInterface extends Stringable
{
	
	/**
	 * Gets whether this slugifier has all its prerequisites confirmed. This 
	 * may be needed if some php extension is needed for the slugifier to work
	 * but is not provided in the current installation of php.
	 * 
	 * @return boolean
	 */
	public function isServiceable() : bool;
	
	/**
	 * Gets the slugified version of the given string, accorging to default
	 * options and specified options.
	 * 
	 * @param ?string $string
	 * @param ?SlugifierOptionsInterface $options
	 * @return string
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string;
	
}

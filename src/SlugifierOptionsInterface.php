<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

use Stringable;

/**
 * SlugifierOptionsInterface interface file.
 * 
 * @author Anastaszor
 */
interface SlugifierOptionsInterface extends Stringable
{
	
	/**
	 * Creates a new options object as the merged options object from this
	 * options and the other given options. 
	 * 
	 * @param ?SlugifierOptionsInterface $other
	 * @return SlugifierOptionsInterface
	 */
	public function mergeWith(?SlugifierOptionsInterface $other) : SlugifierOptionsInterface;
	
	/**
	 * Gets the separator of the string, for splitting word-like characters
	 * streams. The defaults depends of the underlying library, most of the
	 * libraries use '-' as default separator.
	 * 
	 * @return string
	 */
	public function getSeparator() : string;
	
}

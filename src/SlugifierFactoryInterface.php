<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

use Stringable;

/**
 * SlugifierFactoryInterface interface file.
 * 
 * This class is a provider of slugifiers.
 * 
 * @author Anastaszor
 */
interface SlugifierFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new slugifier with the given options.
	 * 
	 * @param ?SlugifierOptionsInterface $slugifierOptions
	 * @return SlugifierInterface
	 */
	public function createSlugifier(?SlugifierOptionsInterface $slugifierOptions = null) : SlugifierInterface;
	
}
